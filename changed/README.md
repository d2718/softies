# changed

Inspired by this [`moreutils` suggestion](https://blog.steve.fi/the_traffic_is_waiting_outside.html).

```text
Determine whether a file has changed.

Usage: changed [OPTIONS] [FILE]

Arguments:
  [FILE]  File to check for changes

Options:
  -f, --forget   Remove the given file from the cache; forget about its state
      --purge    Forget all cached files; purge the cache
  -h, --help     Print help
  -V, --version  Print version
```

## Use

If `changed` doesn't have a cached record of a file, it will report it
as changed, and then cache a hash of it.

```text
~/ $ touch frog.txt
~/ $ changed frog.txt
yes
```

The next time you run it on that file, it'll report whether it's changed.

```text
~/ $ changed frog.txt
no
```

It also returns success (0) if it has changed, and failure (1) if it hasn't.

```text
~/ $ changed frog.txt
no
~/ ! echo $?
1
~/ $ echo "blarg" >frog.txt
~/ $ changed frog.txt
yes
~/ $ echo $?
0
```
