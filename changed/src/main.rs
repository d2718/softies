use std::collections::BTreeMap;
use std::fs::{File, OpenOptions};
use std::path::{Path, PathBuf};

use clap::Parser;

const CACHE_ENV_ARG: &str = "CHANGED_HASH_CACHE";
const BUFF_SIZE: usize = 4 * 1024 * 1024; // 4 MB

/// Remember a file to see if it's changed later.
#[derive(Parser)]
#[command(version, about)]
struct CliArgs {
    /// File to check for changes.
    file: Option<PathBuf>,
    /// Remove the given file from the cache; forget about its state.
    #[arg(short, long)]
    forget: bool,
    /// Forget all cached files; purge the cache.
    #[arg(long)]
    purge: bool,
}

impl CliArgs {
    fn get_target_file(&self) -> PathBuf {
        match self.file {
            None => die("must provide a path to a file"),
            Some(ref file) => match std::fs::canonicalize(file) {
                Err(_) => die(&format!(
                    "unable to canonicalize path {} (may not exist)",
                    file.display()
                )),
                Ok(path) => path,
            },
        }
    }
}

#[derive(Clone, Copy)]
enum ChErr {
    FileNotFound,
    FileNotRwAble,
    Other,
}

impl From<std::io::Error> for ChErr {
    fn from(e: std::io::Error) -> ChErr {
        use std::io::ErrorKind::*;

        match e.kind() {
            NotFound => ChErr::FileNotFound,
            PermissionDenied | IsADirectory => ChErr::FileNotRwAble,
            _ => ChErr::Other,
        }
    }
}

impl From<Box<bincode::ErrorKind>> for ChErr {
    fn from(_: Box<bincode::ErrorKind>) -> ChErr {
        ChErr::FileNotRwAble
    }
}

fn die(msg: &str) -> ! {
    eprintln!("{}", msg);
    std::process::exit(2);
}

fn cache_path() -> PathBuf {
    if let Ok(path) = std::env::var(CACHE_ENV_ARG) {
        return PathBuf::from(path);
    }

    if let Some(mut path) = home::home_dir() {
        path.push(".local");
        path.push("share");
        path.push("changed_cache");
        return path;
    } else {
        die("unable to find a suitable spot for data file");
    }
}

fn read_cache(path: &Path) -> Result<BTreeMap<PathBuf, u64>, ChErr> {
    let mut f = File::open(path)?;
    let hashes: BTreeMap<PathBuf, u64> = bincode::deserialize_from(&mut f)?;
    Ok(hashes)
}

fn write_cache(path: &Path, cache: &BTreeMap<PathBuf, u64>) -> Result<(), ChErr> {
    let mut f = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(path)?;
    bincode::serialize_into(&mut f, cache)?;
    Ok(())
}

fn calc_hash(path: &Path) -> Result<u64, ChErr> {
    use std::io::Read;
    let mut f = File::open(path)?;
    let mut buff = [0u8; BUFF_SIZE];
    let mut hasher = xxhash_rust::xxh64::Xxh64::default();

    loop {
        match f.read(&mut buff)? {
            0 => return Ok(hasher.digest()),
            n => hasher.update(&buff[..n]),
        }
    }
}

fn open_cache() -> BTreeMap<PathBuf, u64> {
    let path = cache_path();
    match read_cache(&path) {
        Ok(cache) => cache,
        Err(ChErr::FileNotFound) => BTreeMap::new(),
        _ => die(&format!(
            "cache file path not read/writable: {}",
            path.display()
        )),
    }
}

fn main() {
    let args = CliArgs::parse();

    if args.purge {
        let p = cache_path();
        if let Err(e) = std::fs::remove_file(&p) {
            die(&format!(
                "unable to remove cache file {}: {}",
                p.display(),
                &e
            ))
        }
    } else if args.forget {
        let target = args.get_target_file();
        let mut cache = open_cache();
        if let Some(_) = cache.remove(&target) {
            let path = cache_path();
            if write_cache(&path, &cache).is_err() {
                die(&format!("unable to write to cache file {}", path.display()));
            }
        }
    } else {
        let target = args.get_target_file();
        let current_hash = match calc_hash(&target) {
            Ok(n) => n,
            Err(ChErr::FileNotFound) => die(&format!("{} not found", target.display())),
            Err(ChErr::FileNotRwAble) => die(&format!("unable to read {}", target.display())),
            Err(_) => die("error calculating hash"),
        };

        let mut cache = open_cache();
        if cache.get(&target) == Some(&current_hash) {
            println!("no");
            std::process::exit(1);
        }
        cache.insert(target, current_hash);
        println!("yes");
        let path = cache_path();
        if write_cache(&path, &cache).is_err() {
            die(&format!("unable to write to cache file {}", path.display()));
        }
        std::process::exit(0)
    }
}
