mod opt;
pub mod times;
pub mod types;
pub mod walk;

use std::{error::Error, sync::Arc};

use opt::Opts;

fn wrapped_main() -> Result<(), Box<dyn Error>> {
    let opts = Opts::new()?;
    let n_threads = std::thread::available_parallelism()
        .map(|u| u.get())
        .unwrap_or(1);
    let base = opts.base.clone();
    let opts = Arc::new(opts);

    walk::run_with_n_workers(opts, n_threads, base);
    Ok(())
}

fn main() {
    if let Err(e) = wrapped_main() {
        eprintln!("{}", &e);
        std::process::exit(1);
    }
}
