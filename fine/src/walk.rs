/*!
Multithreaded directory walking.

The strategy here is to spawn multiple "walker" threads, with the main
thread being a control-and-output thread.

  * The control thread sends directory paths to the walker threads;
    it also holds a lock on stdout.

  * When a walker receives a directory, it iterates through the directory's
    entries, collecting paths that match the search parameters
  * When it encounters an entry that is a _directory_, it sends that
    path back to the control thread.
  * when it finishes iterating through a directory, it sends any matching
    entries back to the control thread.

  * When the control thread receives a directory, it puts it in a queue.
  * When it receives a batch of matching paths, it pops a directory from
    this queue and passes it to the worker thread that just sent said batch.
    It then dumps the bytes of the matching paths it just received through
    the stdout lock that it holds.

  * When there are no more directories in the queue, and all threads are
    idle, we are done; the control thread sends the workers shutdown
    signals, drops the stdout lock, and exits.
*/

use std::collections::VecDeque;
use std::io::Write;
use std::path::PathBuf;
use std::sync::{mpsc, Arc};
use std::thread::{spawn, JoinHandle};

use bstr::ByteSlice;

use crate::opt::Opts;
use crate::types::HasEType;

#[cfg(unix)]
static NEWLINE: &[u8] = b"\n";
#[cfg(windows)]
static NEWLINE: &[u8] = b"\r\n";
#[cfg(wasi)]
static NEWLINE: &[u8] = b"\n";

/// For sending directories to and from walker threads.
struct Dir {
    /// The directory path, obviously.
    path: PathBuf,
    /// The depth below the base directory. This is necessary in case
    /// we're operating under the `--depth` option.
    depth: usize,
}

/// Messages sent from a walker thread to the control thread.
enum FromThread {
    /// A directory's worth of matching paths.
    Matches {
        /// We send the `thread_id` of the walker thread so the control
        /// thread knows which walker thread has just finished, so it
        /// knows where to send the next directory to check.
        thread_id: usize,
        /// A (probably empty) set of matching paths.
        paths: Vec<PathBuf>,
    },
    Dir(Dir),
}

/// Messages sent from the control thread to the walker threads.
enum ToThread {
    /// Instruction to process a directory.
    ///
    /// We recycle the `Vec`s of `PathBuf`s used to report matches
    /// in order to minimize allocations; that's why we're sending
    /// a `Vec` here--it's for the walker to fill with matching paths.
    Dir { dir: Dir, empty: Vec<PathBuf> },
    /// We're done; the thread can stop gracefully.'
    End,
}

/// Handle for sending messages to a walker thread.
struct WorkerHandle {
    thread: JoinHandle<()>,
    to_thread: mpsc::Sender<ToThread>,
}

/// Iterate through the entries in a directory, collecting the matches
/// and sending the subdirectories back to the control thread.
///
/// This is executed by a walker thread every time it receives
/// a directory path from the control thread.
fn process_dir(
    // This worker thread's return pipe for sending `Dir`s back to
    // the control thead.'
    sender: &mpsc::Sender<FromThread>,
    // The directory to process.
    path: PathBuf,
    // The depth of this directory (below the base directory).
    //
    // This is necessary so that when we send `Dir`s back to the
    // control thread, we can report how deep _they_ are.
    depth: usize,
    // Empty `Vec` for us to fill with matching paths.
    mut paths: Vec<PathBuf>,
    // We need to know all the search parameters so we can properly
    // detect matches.
    opts: &Opts,
) -> Vec<PathBuf> {
    let dir_iter = match std::fs::read_dir(&path) {
        Ok(dir_iter) => dir_iter,
        Err(e) => {
            if opts.errors {
                eprintln!("error reading {}: {}", path.display(), &e);
            }
            return paths;
        }
    };

    for res in dir_iter {
        let ent = match opts.unwrap(res) {
            Some(ent) => ent,
            None => continue,
        };

        let file_type = match opts.unwrap(ent.file_type()) {
            Some(ftype) => ftype,
            None => continue,
        };

        let file_path = ent.path();

        // We send all directories we encounter back to the control thread
        // (even if they don't match, obviously).
        if file_type.is_dir() {
            let msg = FromThread::Dir(Dir {
                path: file_path.clone(),
                depth: depth + 1,
            });

            if let Err(e) = sender.send(msg) {
                if opts.errors {
                    eprintln!("{}", &e);
                    return paths;
                }
            }
        }

        let types = opts.types.as_slice();
        if !types.is_empty() {
            if !file_type.is_one(types) {
                continue;
            }
        }

        if let Some(f) = &opts.mod_filter {
            let mtime = match opts
                .unwrap(ent.metadata())
                .and_then(|m| opts.unwrap(m.modified()))
            {
                Some(mtime) => mtime,
                None => continue,
            };

            if !f(mtime) {
                continue;
            }
        }

        if let Some(p) = opts.check_match(&ent) {
            let p = if opts.absolute {
                match opts.unwrap(p.canonicalize()) {
                    Some(p) => p,
                    None => continue,
                }
            } else {
                p
            };

            paths.push(p);
        }
    }

    paths
}

// What gets spawned when we run a worker thread.
//
// It just listens to messages from the control thread: processing
// directories as it receives them, until it gets a `ToThread::End`,
// which is the signal to quit.
fn run_thread(
    // The worker thread needs to know its own `thread_id` so it can be
    // included in batches of matches sent back to the control thread.
    // The _control thread_ needs the `thread_id` in these messages so
    // it knows which thread just finished processing a directory and can
    // thus be given another one.
    //
    // Any errors from either channel means something has gone wrong with
    // the control thread, and we should just bail.
    thread_id: usize,
    incoming: mpsc::Receiver<ToThread>,
    outgoing: mpsc::Sender<FromThread>,
    opts: Arc<Opts>,
) {
    loop {
        match incoming.recv() {
            Err(_) | Ok(ToThread::End) => break,
            Ok(ToThread::Dir { dir, empty }) => {
                let paths = process_dir(&outgoing, dir.path, dir.depth, empty, &opts);
                let msg = FromThread::Matches { thread_id, paths };
                if outgoing.send(msg).is_err() {
                    break;
                }
            }
        }
    }
}

impl WorkerHandle {
    /// Spawn a walker thread and return the handle to it.
    pub fn launch(thread_id: usize, returner: mpsc::Sender<FromThread>, opts: Arc<Opts>) -> Self {
        let (tx, rx) = mpsc::channel::<ToThread>();
        let opts = opts.clone();
        let thread = spawn(move || run_thread(thread_id, rx, returner, opts));
        WorkerHandle {
            thread,
            to_thread: tx,
        }
    }

    /// Send a directory path to a walker thread for it to process.
    ///
    /// We are sending an empty `Vec` to hold the matches because we are
    /// reusing them in an attempt to cut down on allocations.
    pub fn assign(&self, dir: Dir, empty: Vec<PathBuf>) -> Result<(), ()> {
        let msg = ToThread::Dir { dir, empty };
        self.to_thread.send(msg).map_err(|_| ())
    }

    /// Signal to the thread to end gracefully.
    ///
    /// The control thread sends this when there is no more work to do.
    pub fn end(self) {
        self.to_thread.send(ToThread::End).unwrap();
        self.thread.join().unwrap();
    }
}

/// Run the shebang with the given number of "walker" threads.
///
/// The intent is for this to be called with `n_workers` being the output
/// of `std::thread::available_parallelism()`; in the future there may also
/// be an option for a user-specified number of threads.
pub fn run_with_n_workers(opts: Arc<Opts>, n_workers: usize, base: PathBuf) {
    let (tx, receiver) = mpsc::channel::<FromThread>();
    let mut working: Vec<bool> = vec![false; n_workers];
    let workers: Vec<WorkerHandle> = (0..n_workers)
        .into_iter()
        .map(|thread_n| WorkerHandle::launch(thread_n, tx.clone(), opts.clone()))
        .collect();
    let mut path_vecs: Vec<Vec<PathBuf>> = Vec::with_capacity(n_workers);
    let mut dirs_in_waiting: VecDeque<Dir> = VecDeque::with_capacity(n_workers);
    let mut stdout = std::io::stdout().lock();

    workers[0]
        .assign(
            Dir {
                path: base,
                depth: 0,
            },
            Vec::new(),
        )
        .unwrap();
    working[0] = true;

    loop {
        match receiver.recv() {
            Err(e) => {
                eprintln!("search ended early: {}", &e);
                return;
            }
            Ok(FromThread::Dir(dir)) => {
                if let Some(idx) = working.iter().position(|&b| b == false) {
                    let next_dir = if let Some(d) = dirs_in_waiting.pop_front() {
                        if !opts.too_deep(dir.depth) {
                            dirs_in_waiting.push_back(dir)
                        }
                        Some(d)
                    } else {
                        if opts.too_deep(dir.depth) {
                            None
                        } else {
                            Some(dir)
                        }
                    };

                    if let Some(d) = next_dir {
                        if let Ok(_) = workers[idx].assign(d, path_vecs.pop().unwrap_or(Vec::new()))
                        {
                            working[idx] = true;
                        }
                    };
                } else {
                    if !opts.too_deep(dir.depth) {
                        dirs_in_waiting.push_back(dir);
                    }
                }
            }
            Ok(FromThread::Matches {
                thread_id,
                mut paths,
            }) => {
                if let Some(d) = dirs_in_waiting.pop_front() {
                    workers[thread_id]
                        .assign(d, path_vecs.pop().unwrap_or(Vec::new()))
                        .unwrap();
                    for p in paths.drain(..) {
                        if let Some(bytes) = <[u8]>::from_os_str(p.as_os_str()) {
                            stdout.write_all(bytes).unwrap();
                            stdout.write_all(NEWLINE).unwrap();
                        }
                    }
                    path_vecs.push(paths);
                } else {
                    working[thread_id] = false;
                    for p in paths.drain(..) {
                        if let Some(bytes) = <[u8]>::from_os_str(p.as_os_str()) {
                            stdout.write_all(bytes).unwrap();
                            stdout.write_all(NEWLINE).unwrap();
                        }
                    }
                    if working.iter().all(|&b| b == false) {
                        break;
                    } else {
                        path_vecs.push(paths);
                    }
                }
            }
        }
    }

    workers.into_iter().for_each(|w| w.end());
}
